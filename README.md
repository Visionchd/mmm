
# README #
 
 This README has documentation about X-ING Android application.

* In order to edit this document, you can refer to the following document.

[LearnMarkdown](https://bitbucket.org/tutorials/markdowndemo)

## Contents ###

   * App Source Code Repository
   * How do I get set up?
   * Architecture Used in X-ING Android
   * Google Play Store Version
   * Collection of user's data
   * Push notifications
   * CI/CD WorkFlow Of Unit Testing
   * Steps to Create a  Development build of android X-ING project
   * Work flow to publish the App on Google Play Store
   * Who do I talk to?
   * Reference materials
   * Firebase Process

## App Source Code Repository ###
   
  * We used Github to store our X-ING app source code.
  * GitHub is a code hosting platform for collaboration and version control.
  
## How do I get set up? ###
   * Clone project form Github
   * Open project in android studio click on file -> open -> choose project .
   * Install dependencies and sdks as per requirement 
   
## Architecture Used in X-ING Android ###
  
  * We used Android default MVC architecture in X-ING Android.
  
  * MVC stands for Model, View and Controller. MVC separates application into three components - Model, View and Controller.
  
  * Model - the data layer, responsible for managing the business logic and handling network or database API.
  * View - the UI layer - a visualisation of the data from the Model.
  * Controller - the logic layer, gets notified of the user's behaviour and updates the Model as needed.
  
  
  The following diagram illustrates the interaction between Model, View and Controller.
  
  ![alt text](android_mvc.png "Logo Title Text 2")
  
  

## Google PlayStore Version ##
 * Google PlayStore Version is available here 
 (https://play.google.com/store/apps/details?id=com.mobilemarketmonitor.XING)
 * Current version (v1.0.99)
 * Upcoming version (v1.1.02)

## Collection of User's Data ###
 * "GpsPeriodicService" is the service for collecting user's data.
 * On user login, service starts running.
 * Firstly the service/process checks if user is logged in, if not the service stops & returns user to login page.
 * If user is logged in, service/process is scheduled to run again after 3 minutes.
 * Geofence(virtual perimeter set on a real geographic area) is created if the user is at the same place for last 30mins and if the result(Geofence) is true, it will set the value as true in firebase & vice versa.
 * A function checks if the time between the last service & new service is greater than 3 mins or  service is forcefully run by the firebase push .
 * If any of the above condition is satisfied then further process happens otherwise the service is stopped.
 * In further process  firstly the time of this service is stored in local history.
 * Then a process is called to write data is gathered in json format to the phone storage.
 * If acceleration data is available then a json file is created for acceleration, wifi , gsm & battery data & saved in phone memory and after that all the data is cleared.
 * After acceleration file, gps file is created if location is available either from phone network or gps of the phone.
 * After gps file is created activity data is saved in which user motions is stored.
 * When write process is over and if files  available are 30 or more in count then those files are zipped together & uploaded to server.
 * After uploading process is over then the service starts/restarts all the monitoring -
 * Firstly the acceleration process is started if already not working and after that the wifi manager is also started which tracks the wifi states.
 * Acceleration listener/sensor changed event is kept in an array & if sensor changed event is called 40 times then we check when was the last time gps class was called and if the time is more than 37 secs then we call the gps of the phone to record the location data.
 * Gps is called to start the location tracking every 1sec.
 * We have set a timer for 10sec named locationUpdateTimer.
 * If location data is not returned in 10 secs then the locationUpdateTimer stops the gps tracking or else we stop the timer.
 * For every location result, we check whether the location result which we get is not older than 30secs and if the location is older than 30 secs then the location result is not saved.
 * If the location result is not the first location result & if the speed of the location result is equal to zero then we calculate the speed of the location result from distance/time formula & if the speed is in between 0 to 5 we set the speed value.
 * If speed value of the location is greater than 0 then we add the value of speed in a speed array.
 * If the accuracy of the location is less than 100 & speed is greater than 0 then we add the location data to the location tracking class & also update the location to firebase as last location logged.
 * Set the location as last location.
 * now after the loop is over for all results we calculate the average speed of from the speeds array.
 * if average speed is less than 0 or is speeds array size is 0 then a timer is set for 5secs named speedMonitoringTimer to stop the gps tracking & function is returned.
 * If speed array average is gathered in 5secs than this timer is stopped.
 * If average speed is less than 1 then again a timer is set naming accurateTrackingTimer for 30 secs & it will also stop the gps tracking after that & function returned. This timer is called so that gps is not needed if the user is walking.
 * If the average is greater than 1 then the  accurateTrackingTimer is stopped & again locationUpdateTimer is started for 10 secs.
 * After the setup of acceleration listener, gsm listener is started & it gathers the gsm data.
 * After that we call the network for a location result and stops when data is received.
 * When the data is recieved     then the location is saved as the above gps location  is saved.
 * After that we start the Activity recognition service to gather the motion data.

### Flow chart of Data Collection of User's ###

![alt text](FlowChart.png "Logo Title Text 1")

## Push Notifications ###
 * Push notifications are used to wake up the application to keep app running in the background.
 * For Push notification we create a class FirebaseService.java.
 
## CI/CD WorkFlow Of Unit Testing ###

### Introduction of Unit Testing ###

1. Its the testing of business logic written by the developer (not for  UI test).
2. Its  the testing of smallest logical units of our code.
3. for create unit test cases we donot hit API.Instead of hitting API we use Mocking.

### Mocking ###
 
   Mocking is primarily used in unit testing. An object under test may have dependencies on other (complex) objects. To isolate the behavior of the object you want to replace the other objects by mocks that simulate the behavior of the real objects. This is useful if the real objects are impractical to incorporate into the unit test.

   In short, mocking is creating objects that simulate the behavior of real objects.

   * In X-ING Android Project we used a library named as “Mockito” for Mocking. Using this library we can easily mock our methods. 


#### How To Create Unit Test Cases ####
 * First we create a seprate class for unit test.
 * For create a Test Class,first goto that class of which you want to make unit test cases
 * press Alt+Enter on the class name and select "create test",then a pop Up appears.
 * On popup give name to your test class and check "setup@before".then click ok.
 * Then first create instance variables  as per requirement.
 * For mock any class write @Mock annotation above its Variable declaration.
 * Now Goto public void setUp() {}.First line in this method MockitoAnnotations.initMocks(this) is same for every unit Testing  class for initialize our mock variables. 
 * In setup method we mock those methods on which our unit testing Methods are depends.How to mock a method is written below:-
 * For mock a mehtod we use when() for call that method and use then() or thenReturn()to get back return from Method. for eg. when(A.sum(10,20)).thenReturn(30).
 * To Test a Method first write @Test annotation above method name.then call actual method in that Test Method.If any method requires  input as an arguments then pass arguments to the method.
 * Now use assertEqual(),assertTrue()  According to situation for check that Test is pass or fail.

## Steps to Create development Build of android X-ING project ###
* we Create two types of Build.

 1. X-ING(dev).apk
 2. X-ING.apk
 
    

### X-ING(dev).apk Build Steps :

  X-INg(dev).apk is used for testing by developers.
 
 * First Go to module level Build.gradle of Project.
 * Then find productFlavors -> stagging-> Upgrade the version code and version name to latest version.
 * Then find buildTypes -> debug.
 * Add a line "debuggable flase" in debug and sync the project.
 * Then Go to res folder -> xml folder -> open authenticator.xml.
 * In "android:accountType=com.mobilemarketmonitor.XING" line add ".debug" in last. For eg :- This line "com.mobilemarketmonitor.XING" become like this "com.mobilemarketmonitor.XING.debug".
 * Then open AccountHelper.java class,In this class add ".debug" in last to this line "private static final String TYPE_ACCOUNT = "com.mobilemarketmonitor.XING".
 * Now Click on Build in menu bar of android studio.
 * Click on Generate signed Bundle or Apk. Then select APK on from popup window.
 * Click next.
 * To Fill the Key Store Path click on choose existing... button.Then choose your project name from popup Window,then click on App folder in  popup window,
   then select   "com.mobilemarketmonitor.XING.android_key".
 * After this to Fill Key Store Password,Key alias and key password go to your project folder and open the keystore file.and fill key store password,key alias and key password and after filling these fields click on next.
 * Select Staging Debug from popup window.
 * Then click on Finish.
 
 Build X-ING(dev).apk is ready.
 
#### Steps to create hockeylink of X-ING(dev).apk ###
 
 * Open hockeyapp.net in browser of pc.
 * Then upload X-ING(dev).apk over there.and download link of apk.

#### X-ING.apk Build Steps :

 x-ing.apk is used for production on Google Play Store.

* First Go to module level Build.gradle of Project.
* Then find productFlavors -> production -> Upgrade the version code and version name to latest version.
* Now Click on Build in menu bar of android studio.
* Click on Generate signed Bundle or Apk. Then select APK on from popup window.
* Click next.
* Fill the Key Store Path click on choose existing... button.Then choose your project name from popup Window,then click on App folder in  popup window, then select   "com.mobilemarketmonitor.XING.android_key".
* After this to Fill Key Store Password,Key alias and key password go to your project folder and open the keystore file.and fill key store password,key alias and key password and after filling these fields click on next.
* Select production Release from popup window.
* Then click on Finish. 

Build X-ING.apk is ready.
       


## Work flow to publish the App on Google Play Store ###

   On Google Play Store When we upload a X-ING App.we have three options
    
#### Alpha Testing####
   * It is conducted within the organization and tested by an individual developer or a team of developers or testers. This testing is closed for public.
#### Beta Testing ####
   * It is conducted by the end users who are not programmers, software engineers or testers. This testing may be open for public.
#### Production ####
   * It is the Production of  app  on Google Play Store for public.
      
   we used two options Beta testing and Production for x-ing  among three options.

    
  * For publish app on google play store go to Google Play Console.
  * Now we have Beta and Production options,click on Manage Tab of that option  to upload your Apk.
  * After selecting Manage Tab, Now we have options for upload Apk whether we add Apk from library or browse from pc.
  * Getting successful upload of Apk, we can give version to the Apk.
  * In last give  name to the release  and write some release  description, and click on save.
  
## Who do I talk to? ###
 * Repo owner or admin
 [Puneet](mailto:puneet@mobilemarketmonitor.com)
 * Other community or team contact
 [Jorge Santos](mailto:jorge@mobilemarketmonitor.com)
 [Fang Zhao](mailto:fang@mobilemarketmonitor.com)


## Reference Materials ###

 * Architecture used in x-ing :- https://androvaid.com/android-mvc-example/
 * Unit Test : - https://www.journaldev.com/22674/android-unit-testing-junit4
 
## Firebase Process ###
 
 
